const functions = require('firebase-functions');
import getBriefing from './get-briefing';

// Create and Deploy Your First Cloud Functions
// https://firebase.google.com/docs/functions/write-firebase-functions

exports.getBriefing = functions.https.onRequest(getBriefing);
