import React from 'react';

import {Provider} from 'react-redux';

import MomentUtils from 'material-ui-pickers/utils/moment-utils';
import MuiPickersUtilsProvider from 'material-ui-pickers/utils/MuiPickersUtilsProvider';

import CssBaseline from 'material-ui/CssBaseline';

// import LoginScreen from './login/Login';
// import BriefsScreen from '../brief/BriefsScreen';
// import Brief from '../brief/Brief';
// import Story from './../story/Story';
// import BrieflyRouter from './routes';

import {store, BrieflyRouter} from './reducers';

const App = () => (
  <Provider store={store}>
    <MuiPickersUtilsProvider utils={MomentUtils}>
      <div>
        <CssBaseline />
        <BrieflyRouter />
      </div>
    </MuiPickersUtilsProvider>
  </Provider>
);

export default App;
