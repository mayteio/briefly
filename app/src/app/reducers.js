import React from 'react';

import {createStore, combineReducers, applyMiddleware} from 'redux';
import {Provider} from 'react-redux';

import createHistory from 'history/createBrowserHistory';
import {Route, Switch} from 'react-router';

import {ConnectedRouter, routerReducer, routerMiddleware, push} from 'react-router-redux';

import LoginScreen from '../login/Login';
import BriefsScreen from '../brief/BriefsScreen';
import Brief from '../brief/Brief';

import Story from '../story/Story';

import BriefReducer from '../brief/BriefReducer';

const history = createHistory();
const middleware = routerMiddleware(history);

const store = createStore(
    combineReducers({
        briefs: BriefReducer,
        router: routerReducer
    }),
    applyMiddleware(middleware)
);

const BrieflyRouter = () => {
    return (
        <ConnectedRouter history={history}>
            <Switch>
                <Route exact path="/login" component={LoginScreen} />
                <Route exact path="/briefs/:id" component={Brief} />
                <Route path="/briefs/:id/story" component={Story} />
                <Route exact path="/briefs" component={BriefsScreen} />
            </Switch>
        </ConnectedRouter>
    )
}

export {store, BrieflyRouter}