import React, {Component} from 'react';
import PropTypes from 'prop-types';
import {withStyles} from 'material-ui/styles';
import Grid from 'material-ui/Grid';
import Paper from 'material-ui/Paper';
import Button from 'material-ui/Button';
import {connect} from 'react-redux';
import {push} from 'react-router-redux';

function changeScreen(screen){
// 
}

const styles = theme => ({
    root: {
        flexGrow: 1,
        height: '100%',
        padding: 24
    },
    paper: {
        height: '100%',
        padding: theme.spacing.unit * 2,
        textAlign: 'center'
    }
});

const ConnectedLoginScreen = (props) => {
    const { classes, goToApp } = props;

    return (
        <div className={classes.root}>
            <Grid container  spacing={24}>
                <Grid item xs={12}>
                    <Paper className={classes.paper}>
                        <h1>Briefly</h1>
                        <Button 
                            variant="raised" 
                            color="primary"
                            onClick={goToApp}
                        >
                            Login with Facebook
                        </Button>
                    </Paper>
                </Grid>
            </Grid>
        </div>
    );
}

const mapDispatchToProps = dispatch => {
    return {
        goToApp: () => dispatch(push('/briefs'))
    }
}

const LoginScreen = connect(null, mapDispatchToProps)(ConnectedLoginScreen);

LoginScreen.propTypes = {
    classes: PropTypes.object.isRequired
}

export default withStyles(styles)(LoginScreen);