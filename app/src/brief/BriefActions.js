import uuid from 'uuid/v4';

import {
    GET_BRIEFS_FAILURE,
    GET_BRIEFS_SUCCESS,
    GET_BRIEFS_LOADING,
    ADD_BRIEF,
    ADD_DRAFT_BRIEF
} from './BriefConstants';

import {getBriefs} from './BriefHelper';


export function addBrief(brief) {
    return {
        type: ADD_BRIEF,
        payload: brief
    }
}

export function addDraftBrief(brief){
    new Promise()
    .then()
    return {
        type: ADD_DRAFT_BRIEF,
        brief: brief
    }
}

export function getBriefsHasErrored(bool){
    return {
        type: GET_BRIEFS_FAILURE,
        hasError: bool
    };
}

export function getBriefsHasSucceeded(briefs){
    return {
        type: GET_BRIEFS_SUCCESS,
        briefs
    }
}

export function getBriefsIsLoading(bool){
    return {
        type: GET_BRIEFS_LOADING,
        isLoading: bool
    }
}

export function briefsFetchData(url){
    return dispatch => {
        dispatch(getBriefsIsLoading(true));

        getBriefs
            .then(response => {
                if(!response.ok){
                    throw Error(response.statusText)
                }

                dispatch(getBriefsIsLoading(false));

                return response;
            })
            // .then(response => response.json())
            .then(briefs => dispatch(getBriefsHasSucceeded(briefs)))
            .catch(() => dispatch(getBriefsHasErrored(true)));
    }
}