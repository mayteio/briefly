import React from 'react';
import PropTypes from 'prop-types';
import {withStyles} from 'material-ui/styles';
import Paper from 'material-ui/Paper';
import Typography from 'material-ui/Typography';

const styles = theme => ({
    paper: {
        padding: theme.spacing.unit * 2
    }
});

const BriefItem = (props) => {

    const {classes} = props;

    return (
        <Paper className={classes.paper}>
            <Typography variant="title">{props.brief.date}</Typography>
            <Typography variant="caption">{props.brief.stories} stories</Typography>
        </Paper>
    )
}

BriefItem.propTypes = {
    brief: PropTypes.object,
    classes: PropTypes.object.isRequired
};

export default withStyles(styles)(BriefItem);