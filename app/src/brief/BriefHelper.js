/**
 * Briefs API. Probably replace this with 
 * redux thunk in actions.
 */

let briefs = [
    {
        date: '12th March 2018',
        stories: 3
    },
    {
        date: '13th March 2018',
        stories: 4
    },
];

const getBriefs = () => {
    return new Promise(resolve => resolve(briefs))
}

const getBrief = () => {
    return new Promise(resolve => resolve(briefs[0]))
}

export {getBriefs, getBrief};