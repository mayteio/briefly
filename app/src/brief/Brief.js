import React, {Component} from 'react';
import PropTypes from 'prop-types';
import {withStyles} from 'material-ui/styles';
import AppBar from 'material-ui/AppBar';
import Toolbar from 'material-ui/Toolbar';
import Typography from 'material-ui/Typography';
import Button from 'material-ui/Button';
import IconButton from 'material-ui/IconButton';
import CloseIcon from 'material-ui-icons/Close';
import AddIcon from 'material-ui-icons/Add';
import {DatePicker} from 'material-ui-pickers';

import {connect} from 'react-redux';
import {replace} from 'react-router-redux';

import StoryList from '../story/StoryList';
import {getStorys} from '../story/StoryHelper';

import {addBrief} from './BriefActions';

const styles = theme => ({
    root: {
        flexGrow: 1
    },
    flex: {
        flex: 1
    },
    menuButton: {
        marginLeft: -12,
        marginRight: 20
    },
    textFieldInput: {
        ...theme.typography.title,
        color: '#fff'
    },
    fab: {
        marginLeft: -12,
        marginRight: 20,
        marginBottom: -62
    }
});

class ConnectedBrief extends Component {

    state = {
        date: new Date(),
        stories: []
    };

    componentDidMount(){
        if(this.props.match.params.id === 'new'){
            this.setState({brief: this.props.draft})
        } else {
            this.setState({brief: this.props.brief});
        }
    }

    handleDateChange = selectedDate => {
        this.setState({
            ...this.state,
            brief: {
                ...this.state.brief,
                date: selectedDate
            }
        });

    }

    handleSave = () => {
        this.props.addBrief({
            const {date, }
        });
    }

    render(){
        const {
            classes, 
            cancelNewBrief,
            addBrief
        } = this.props;
        const {selectedDate, stories} = this.state;
        
        return (
            <div className={classes.root}>
                <AppBar position="static">
                    <Toolbar>
                        <IconButton 
                            className={classes.menuButton} 
                            color="inherit"
                            onClick={cancelNewBrief}
                        >
                            <CloseIcon />
                        </IconButton>
                        <Typography variant="title" color="inherit" className={classes.flex}>
                            &nbsp;
                        </Typography>
                        <Button color="inherit" onClick={this.handleSave}>Save</Button>
                    </Toolbar>
                    <Toolbar>
                        <div className={classes.flex}>
                            <DatePicker
                                value={selectedDate}
                                onChange={this.handleDateChange}
                                format="MMMM Do YYYY"
                                disablePast={true}
                                InputProps={{
                                    disableUnderline: true,
                                    classes: {
                                        input: classes.textFieldInput
                                    }
                                }} 
                            />
                        </div>
                    </Toolbar>
                </AppBar>
                <StoryList stories={stories} />
                <Button variant="fab" color="secondary" aria-label="add" className={classes.fab}>
                    <AddIcon />
                </Button>
            </div>
        )
    }
}

const mapStateToProps = (state, ownProps) => {
    return {

    }
}

const mapDispatchToProps = dispatch => {
    return {
        cancelNewBrief: () => dispatch(replace('/briefs')),
        addBrief: brief => dispatch(addBrief(brief))
    }
}

const Brief = connect(mapStateToProps, mapDispatchToProps)(ConnectedBrief);


Brief.propTypes = {
    classes: PropTypes.object
};

export default withStyles(styles)(Brief)