import React from 'react';
import PropTypes from 'prop-types';
import {withStyles} from 'material-ui/styles';
import Grid from 'material-ui/Grid';
import BriefItem from './BriefItem';

const styles = theme => ({
    root: {
        flexGrow: 1,
        padding: theme.spacing.unit * 2 
    }
});

const BriefList = (props) => {

    const {classes} = props;

    const briefs = props.briefs.map(brief => {
        return (
            <Grid item xs="12">
                <BriefItem brief={brief} />
            </Grid>
        )
    });

    return (
        <div className={classes.root}>
            <Grid container spacing={24}>
                {briefs}
            </Grid>
        </div>
    )
}

BriefList.propTypes = {
    briefs: PropTypes.array,
    classes: PropTypes.object
};

export default withStyles(styles)(BriefList);