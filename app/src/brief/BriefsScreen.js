import React, {Component} from 'react';
import PropTypes from 'prop-types';
import {withStyles} from 'material-ui/styles';
import AppBar from 'material-ui/AppBar';
import Toolbar from 'material-ui/Toolbar';
import Typography from 'material-ui/Typography';
import Button from 'material-ui/Button';
import IconButton from 'material-ui/IconButton';
import MenuIcon from 'material-ui-icons/Menu';
import AddIcon from 'material-ui-icons/Add';
import Grid from 'material-ui/Grid';

import {connect} from 'react-redux';
import {push} from 'react-router-redux';

import BriefList from './BriefList';

const styles = theme => ({
    root: {
        flexGrow: 1
    },
    flex: {
        flex: 1
    },
    menuButton: {
        marginLeft: -12,
        marginRight: 20
    },
    fab: {
        position: 'absolute',
        bottom: theme.spacing.unit * 2,
        right: theme.spacing.unit * 2
    }
})

const ConnectedBriefsScreen = (props) => {

    const { 
        classes, 
        briefs,
        addNewBrief
     } = props;

    return (
        <div className={classes.root}>
            <AppBar position="static">
                <Toolbar>
                    <IconButton className={classes.menuButton} color="inherit">
                        <MenuIcon />
                    </IconButton>
                    <Typography variant="title" color="inherit">
                        Briefly
                    </Typography>
                </Toolbar>
            </AppBar>
            
            <BriefList briefs={briefs} />

            <Button 
                variant="fab" 
                className={classes.fab} 
                color="secondary"
                onClick={addNewBrief}>
                <AddIcon />
            </Button>
            
        </div>
    )
}

const mapStateToProps = state => {

    return {
        briefs: state.briefs.items
    }
}

const mapDispatchToProps = dispatch => {
    return {
        addNewBrief: () => dispatch(push('/briefs/new'))
    }
}

const BriefsScreen = connect(mapStateToProps, mapDispatchToProps)(ConnectedBriefsScreen);

BriefsScreen.propTypes = {
    classes: PropTypes.object.isRequired
}

export default withStyles(styles)(BriefsScreen);