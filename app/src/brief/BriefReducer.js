import {ADD_BRIEF, ADD_DRAFT_BRIEF} from './BriefConstants';

/**
 * REDUCER
 */
const initialState = {
    items: [],
    draft: {}
}

const BriefReducer = function(state = initialState, action) {
    switch(action.type){
        case ADD_BRIEF :
            return {
                draft: {},
                items: [...state.items, action.payload]
            };

        case ADD_DRAFT_BRIEF :
            return {
                ...state,
                draft: action.payload
            };
            
        default: 
            return state;
    }
}

export default BriefReducer;