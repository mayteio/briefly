import React from 'react';
import PropTypes from 'prop-types';
import {withStyles} from 'material-ui/styles';

import Card, {CardActions, CardContent, CardMedia} from 'material-ui/Card';
import Button from 'material-ui/Button';
import Typography from 'material-ui/Typography';

const styles = theme => ({
    media: {
        height: 200
    }
});

const StoryItem = (props) => {
    const {classes, story} = props;

    return (
        <div>
            <Card>
                <CardMedia
                    className={classes.media}
                    image={story.image}
                    title={story.title}
                />
                <CardContent>
                    <Typography variant="headline" component="h2">
                        {story.title}
                    </Typography>
                    <Typography component="p">
                        {story.description}
                    </Typography>
                </CardContent>
                <CardActions>
                    <Button size="small" color="primary">
                        Remove
                    </Button>
                    <Button size="small" color="primary">
                        Edit
                    </Button>
                </CardActions>
            </Card>
        </div>
    )
}

StoryItem.propTypes = {
    story: PropTypes.object,
    classes: PropTypes.object
}

export default withStyles(styles)(StoryItem);