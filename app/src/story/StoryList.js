import React from 'react';
import PropTypes from 'prop-types';
import {withStyles} from 'material-ui/styles';
import Grid from 'material-ui/Grid';
import StoryItem from './StoryItem';

let styles = theme => ({
    root: {
        flexGrow: 1,
        padding: theme.spacing.unit * 2
    }
});

const StoryList = (props) => {

    const {classes} = props;
    const stories = props.stories.map((story, index) => {
        return (
            <Grid item xs={12} md={4} key={index}>
                <StoryItem story={story} />
            </Grid>
        )
    });

    return (
        <div className={classes.root}>
            <Grid container spacing={24}>
                {stories}
            </Grid>
        </div>
    )
}

StoryList.propTypes = {
    storys: PropTypes.array,
    classes: PropTypes.object
};

export default withStyles(styles)(StoryList);