import React, {Component} from 'react';
import PropTypes from 'prop-types';
import {withStyles} from 'material-ui/styles';

import Grid from 'material-ui/Grid';
import AppBar from 'material-ui/AppBar';
import Toolbar from 'material-ui/Toolbar';
import Typography from 'material-ui/Typography';
import Button from 'material-ui/Button';
import Divider from 'material-ui/Divider';
import IconButton from 'material-ui/IconButton';
import CloseIcon from 'material-ui-icons/Close';

import TextField from 'material-ui/TextField';

import {getStory} from './StoryHelper';

const styles = theme => ({
    root: {
        flexGrow: 1,
        padding: 20
    },
    flex: {
        flex: 1
    },
    menuButton: {
        marginLeft: -12,
        marginRight: 20
    },

    titleFieldInput: {
        ...theme.typography.title,
        color: '#fff'
    },
    body: {
        padding: theme.spacing.unit * 2
    }
});

class Story extends Component {

    state = {
        story: {
            title: ''
        }
    }

    async componentDidMount(){
        console.log(this);
    }

    handleChange = name => event => {
        this.setState({
            story : {
                [name]: event.target.value
            }
        })
    }

    render(){
        const {classes} = this.props;
        const {story} = this.state;

        return (
            <form noValidate autoComplete="off">
                <AppBar position="static">
                    <Toolbar>
                        <IconButton className={classes.menuButton} color="inherit">
                            <CloseIcon />
                        </IconButton>
                        <Typography variant="title" color="inherit" className={classes.flex}>
                            &nbsp;
                        </Typography>
                        <Button color="inherit">Add</Button>
                    </Toolbar>
                    <Toolbar>
                        <div className={classes.flex}>
                            <TextField
                                value={story.title}
                                onChange={this.handleChange('title')}
                                id="title"
                                name="title"
                                required={true}
                                placeholder="Story Title"
                                InputProps={{
                                    disableUnderline: true,
                                    classes: {
                                        input: classes.titleFieldInput
                                    }
                                }} 
                            />
                        </div>
                    </Toolbar>
                </AppBar>
                <div className={classes.root}>
                    <Grid container>
                        <Grid item xs={12} className={classes.body}>
                            <TextField
                                label="description"
                                fullWidth={true}
                                id="description"
                                name="description"
                                onChange={this.handleChange('description')}
                                multiline={true}
                                placeholder="What should this brief say?"
                                required={true}
                            />
                            <Divider />
                        </Grid>
                    </Grid>
                </div>
            </form>
        )
    }
}

const mapDispatchToProps = dispatch => {
    addStory: (story, ) => {
        dispatch()
    }
}

Story.propTypes = {
    classes: PropTypes.object
}

export default withStyles(styles)(Story);