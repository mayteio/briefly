let storys = [
    {
        image: 'https://source.unsplash.com/random',
        audio: 'https://audiourl.com',
        title: 'Puppies Epic Battle',
        description: 'Two puppies battle it out for their owner’s affection. You won’t believe what happens next!',
        alexa: true,
        home: false
    },
    {
        image: 'https://source.unsplash.com/random',
        audio: 'https://audiourl.com',
        title: 'Kittens are bros',
        description: 'Kittens become bros with these incredibly simple steps!',
        alexa: true,
        home: true
    },
    {
        image: 'https://source.unsplash.com/random',
        audio: 'https://audiourl.com',
        title: 'Kids fall over',
        description: 'If you haven\'t seen the ChildrenFallingOver reddit you\'re missing out.',
        alexa: true,
        home: true
    }
];

const getStorys = () => {
    return new Promise(resolve => resolve(storys))
}

const getStory = () => {
    return new Promise(resolve => resolve(storys[0]));
}

export {getStorys, getStory};